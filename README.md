Why am I building this project
- To get familiar with GitLab workflow
- To apply and strengthen skills using Git
- To become more familiar with GitLab platform

This is my training link:
https://gitlab-com.gitlab.io/customer-success/professional-services-group/trainings/basics/deck/both.html#/26

